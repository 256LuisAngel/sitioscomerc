<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FrontEnd
{
	//home
	public function indexAction(Application $app) {
    	return '';
			
	}

	//funcion para el registro de los clientes
	public function registro(Application $app)    {
		
	$clave = 'Clave';
	$nombre = 'Nombre';
	$apellido = 'Apellido';
	$correo = 'Correo';
	$calle = 'Calle';
	$colonia = 'Colonia';
	$cp = 'CP';
	$ciudad = 'Ciudad';
	$pais = 'Pais';
	$telefono = 'Telefono';

	return $app['twig']->render('registro.twig', array(
		'clave' => $clave,
		'nombre' => $nombre,
		'apellido' => $apellido,
		'correo' => $correo,
		'calle' => $calle,
		'colonia' => $colonia,
		'cp' => $cp,
		'ciudad' => $ciudad,
		'pais' => $pais,
		'telefono' => $telefono
		));
		
	}
	
	//funcion para añadir los registros a la BD
	public function clienteRegistrado (Application $app, Request $request) {
	
	$_clave = $request->get('claveIngresado');
	$_nombre = $request->get('nombreIngresado');					// aqui se recuperan los datos ingresados
	$_apellido = $request->get('apellidoIngresado');					 
	$_correo = $request->get('correoIngresado');					 
	$_calle = $request->get('calleIngresado');
	$_colonia = $request->get('coloniaIngresado');
	$_cp = $request->get('zipIngresado');
	$_ciudad = $request->get('ciudadIngresado');
	$_pais = $request->get('paisIngresado');
	$_telefono = $request->get('telefonoIngresado');
	
		
    $app['db']->insert ('Clientes', array(						//aqui se añaden a la BD
        'CLAVE' => $_clave,
		'Nombre' => $_nombre,
		'Apellido' => $_apellido,
		'Correo' => $_correo,
		'Calle' => $_calle,
		'Colonia' => $_colonia,
		'CP' => $_cp,
		'CIUDAD' => $_ciudad,
		'PAIS' => $_pais,
		'TELEFONO' => $_telefono
      ));

	  return 'datos ingresados a la BD';
	
	} 

	
} // fin de clase FrontEnd


	
	
